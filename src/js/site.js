const techSelectionContainer = getTechSelectionContainer()
const techs = getAllTechs()

let selectedTechIdCount = 0
let selectedTechIds = new Map()

for (i = 0; i < techs.length; i++) {
    const t = techs[i]
    const element = techToElement(t)
    techSelectionContainer.appendChild(element)

    element.querySelector("input").addEventListener("change", (event) => { 
        const parentNode = event.target.parentNode.parentNode
        if (event.target.checked) 
        {
            selectedTechIds[t] = 1
            selectedTechIdCount++
            if (!parentNode.classList.contains("input-checked"))
                parentNode.classList.add("input-checked")
        }
        else
        {
            delete selectedTechIds[t]
            selectedTechIdCount--
            if (parentNode.classList.contains("input-checked"))
                parentNode.classList.remove("input-checked")
        }

        if (selectedTechIdCount == 0) 
            enableAllProjects()
        else
            updateFilteredProjects(selectedTechIds)
    })
}

/**
 * Updates the projects html based on the filteres applied. It will automatically enable/disable each section
 * under #projects based on the applied filters
 * @param {Map} filters Map of filters, simply containing a hash of each selected tech
 */
function updateFilteredProjects(filters) {
    const projects = document.querySelectorAll("#projects > section")

    for (let i = 0; i < projects.length; i++) {
        const proj = projects[i]
        const techsUsed = proj.querySelectorAll("ul.techs-used > li")

        let wasFound = false;
        for (let j = 0; j < techsUsed.length; j++) {
            const tu = techsUsed[j]
            if (tu.textContent in filters) {
                wasFound = true;
                if (!tu.classList.contains("searched"))
                    tu.classList.add("searched")
            }
            else {
                if (tu.classList.contains("searched"))
                    tu.classList.remove("searched")
            }
        }

        if (wasFound && proj.classList.contains("disabled")) {
            proj.classList.remove("disabled")
        }
        if (!wasFound && !proj.classList.contains("disabled")) {
            proj.classList.add("disabled")
        }
    }
}

/**
 * Removes the ".disabled" CSS selectors from ALL section elements inside #projects, as well as any ".searched" classes
 */
function enableAllProjects() {
    const projects = document.querySelectorAll("#projects > section.disabled")
    for (let i = 0; i < projects.length; i++) 
        projects[i].classList.remove("disabled")

    const searched = document.querySelectorAll("#projects .searched")
    for (let i = 0; i < searched.length; i++)
        searched[i].classList.remove("searched")
}

/**
 * Gets the <div> element containing the id of #tech-selection. In this case, 'tech' referrs to 
 * a technology/skill like PHP, C#, etc
 */
function getTechSelectionContainer() {
    return document.querySelector("div#tech-selection")
}

/**
 * Gets all list items that have the CSS selector 'ul.techs-used', and removes any duplicates where 'tech'
 * refers to some kind of technology I have used, such as PHP
 * @returns {String[]} An array of strings, sorted in alphabetical order
 */
function getAllTechs() {
    const techsHash = {}
    const techsList = []
    const foundTechs = document.querySelectorAll("ul.techs-used li")

    for (let i = 0; i < foundTechs.length; i++) {
        const t = foundTechs[i].textContent
        if (!(t in techsHash)) {
            techsHash[t] = 1
            techsList.push(t)
        }
    }

    return techsList.sort() // I tried to find a way to return just the keys in the hash map, but couldn't :(
}

/**
 * Converts a tech string (from getAllTechs()) into a checklist element that can be appended to 
 * getTechSelectionContainer(). The id of the checkbox being created is simply `tech-${t.toLowerCase()}`,
 * (it will remove spaces and other characters too)
 * @param {String} t The technology name, such as PHP
 */
function techToElement(t) {
    const div = document.createElement("div")
    const label = document.createElement("label")
    const checkbox = document.createElement("input")
    const labelText = document.createTextNode(t)

    const id = stripSpecial(`tech-${t.toLowerCase()}`)
    checkbox.type = "checkbox"
    checkbox.id = id
    label.setAttribute("for", id)

    label.appendChild(checkbox)
    label.appendChild(labelText)
    div.appendChild(label)
    div.classList.add("fancy-border")

    return div
}

/** Strips special characters from t */
function stripSpecial(t) {
    let newT = t.replace(/[., ]+/g, "-")
    newT = newT.replace(/\+\++/g, 'pp') 
    newT = newT.replace(/#+/g, "sharp")
    return newT
}
